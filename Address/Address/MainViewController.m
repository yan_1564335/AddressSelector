//
//  MainViewController.m
//  Address
//
//  Created by 胡岩 on 16/3/3.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import "MainViewController.h"
#import "AddressViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(100, 100, 100, 100);
    button.center = self.view.center;
    [self.view addSubview:button];
    button.backgroundColor = [UIColor orangeColor];
    [button setTitle:@"点一下" forState:UIControlStateNormal];
}

- (void)touchUp:(UIButton *)button {
    AddressViewController *address = [[AddressViewController alloc] initWithBlock:^(CityModel *model) {
        NSLog(@"%@", model.cityname);
    }];
    [self.navigationController pushViewController:address animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
