//
//  SingerListCollectionView.h
//  YanMusicPlayer
//
//  Created by 1564335 on 15-6-12.
//

#import <UIKit/UIKit.h>

@protocol TouchTableViewDelegate <NSObject>

@optional

- (void)tableView:(UICollectionView *)tableView
     touchesBegan:(NSSet *)touches
        withEvent:(UIEvent *)event;

- (void)tableView:(UICollectionView *)tableView
 touchesCancelled:(NSSet *)touches
        withEvent:(UIEvent *)event;

- (void)tableView:(UICollectionView *)tableView
     touchesEnded:(NSSet *)touches
        withEvent:(UIEvent *)event;

- (void)tableView:(UICollectionView *)tableView
     touchesMoved:(NSSet *)touches
        withEvent:(UIEvent *)event;

@end

@interface SingerListCollectionView : UICollectionView

@property (nonatomic, assign) id<TouchTableViewDelegate> touchDelegate;

@end



