//
//  AddressViewController.h
//  Address
//
//  Created by 胡岩 on 16/2/24.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityModel.h"

typedef void (^Block) (CityModel *model);

@interface AddressViewController : UIViewController

- (instancetype)initWithBlock:(Block)block;

@end
