//
//  AddressView.m
//  Address
//
//  Created by 胡岩 on 16/2/24.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import "AddressView.h"
#import "AddressViewTableViewCell.h"

@interface AddressView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *names;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) NSInteger currentSelector;

@end

@implementation AddressView

- (instancetype)initWithFrame:(CGRect)frame names:(NSArray *)names {
    self = [super initWithFrame:frame];
    if (self) {
        _names = names;
        _tableView = [[UITableView alloc ]initWithFrame:self.bounds];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.userInteractionEnabled = NO;
        [self addSubview:_tableView];
        _tableView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)reloadWithNames:(NSArray *)names {
    _names = [NSMutableArray arrayWithArray:names];
    [_tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    AddressViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[AddressViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.text = _names[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _names.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.frame.size.height / _names.count;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    NSInteger num = point.y / (self.frame.size.height / _names.count);
    if ([_delegate respondsToSelector:@selector(addressView:didSelectNum:)]) {
        [_delegate addressView:self didSelectNum:num];
    }
    _currentSelector = num;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    NSInteger num = point.y / (self.frame.size.height / _names.count);
    if ([_delegate respondsToSelector:@selector(addressView:didSelectNum:)] && num != _currentSelector) {
        _currentSelector = num;
        [_delegate addressView:self didSelectNum:num];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    NSInteger num = point.y / (self.frame.size.height / _names.count);
    if ([_delegate respondsToSelector:@selector(addressView:didSelectNum:)]) {
        [_delegate addressView:self didSelectNum:num];
    }
}

@end
