//
//  AddressViewTableViewCell.h
//  Address
//
//  Created by 胡岩 on 16/2/25.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressViewTableViewCell : UITableViewCell


@property (nonatomic, strong) NSString *text;

@end
