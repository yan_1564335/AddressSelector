//
//  AddressViewTableViewCell.m
//  Address
//
//  Created by 胡岩 on 16/2/25.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import "AddressViewTableViewCell.h"

@interface AddressViewTableViewCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation AddressViewTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _label = [[UILabel alloc] init];
        [self.contentView addSubview:_label];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _label.frame = self.contentView.bounds;
    _label.textAlignment = NSTextAlignmentCenter;
    _label.font = [UIFont boldSystemFontOfSize:15];
}

- (void)setText:(NSString *)text {
    _label.text = text;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
