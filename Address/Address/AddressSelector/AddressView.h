//
//  AddressView.h
//  Address
//
//  Created by 胡岩 on 16/2/24.
//  Copyright © 2016年 MagicalYan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddressView;
@protocol AddressViewDelegate <NSObject>

- (void)addressView:(AddressView *)addressView didSelectNum:(NSInteger)number;

@end

@interface AddressView : UIView

@property (nonatomic, weak) id<AddressViewDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame names:(NSArray *)names;

- (void)reloadWithNames:(NSArray *)names;

@end
