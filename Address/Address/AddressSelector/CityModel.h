//
//  CityModel.h
//  YiFoLi
//
//  Created by 胡岩 on 15/12/18.
//  Copyright © 2015年 胡岩. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityModel : NSObject
@property (nonatomic, retain) NSNumber *citycode;//编码
@property (nonatomic, retain) NSNumber *cityid;//id
@property (nonatomic, retain) NSString *cityname;//名称
@property (nonatomic, retain) NSNumber *provincecode;//所属城市编码
- (id)initWithDictionary:(NSDictionary *)dic;

@end
