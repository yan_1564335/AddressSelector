# AddressSelector
select address

https://github.com/MagicalYan/AddressSelector

一个简单的地区选择页面, 个人感觉做成本地数据库会更好一些, 现在支持侧边快速选择, 点击方法可以自己添加, 具体功能可以根据用途来更改。

![alt text](https://raw.githubusercontent.com/MagicalYan/AddressSelector/master/Address/add.gif)

新增查询功能, 如果要是写个数据库就更好了,😂
感兴趣的可以改改代码, 添加个什么热门城市, 本地城市什么的
